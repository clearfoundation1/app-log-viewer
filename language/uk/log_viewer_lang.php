<?php

$lang['log_viewer_app_description'] = 'Додаток «Перегляд журналу» забезпечує відображення всіх файлів системного журналу. Перегляд файлів журналів і пошук є важливим та, як правило, першим кроком у вирішенні проблем із вашою системою.';
$lang['log_viewer_app_name'] = 'Перегляд журналу';
$lang['log_viewer_display'] = 'Перегляд';
$lang['log_viewer_entry'] = 'Запис у журналі';
$lang['log_viewer_export_to_file'] = 'Експортувати у файл';
$lang['log_viewer_file'] = 'Файл журналу';
$lang['log_viewer_filter'] = 'Фільтр';
$lang['log_viewer_invalid_log_file'] = 'Недійсний файл журналу.';
$lang['log_viewer_logs'] = 'Журнал';
$lang['log_viewer_result_set_too_big'] = 'Набір результатів більше, ніж максимум, який можна відобразити. Будь ласка, скористайтесь фільтром, щоб зменшити обсяг даних.';
$lang['log_viewer_show_full_line'] = 'Показати весь рядок';
